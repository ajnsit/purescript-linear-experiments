module Main where

import Control.Applicative (pure)
import Control.Bind (bind, discard)
import Data.Function (($))
import Data.Show (show)
import Data.Unit (Unit)
import Effect (Effect)
import Effect.Console (log)
import Linear (linear, use)
import Quicksort (qsort)

foreign import mkLargeRandomArray :: Int -> Int -> Effect (Array Int)

main :: Effect Unit
main = do
  -- 10000 elements, between 0-40
  original <- mkLargeRandomArray 100 40
  -- let original = [ 1, 2, 3, 4, 5 ]
  let larr = linear original
  sorted <- pure $ qsort larr
  log $ show $ use sorted
  -- At this point the original array should be the same as the modified array
  log $ show original

