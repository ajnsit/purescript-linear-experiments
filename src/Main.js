export function mkLargeRandomArray(len) {
  return function(max) {
    return function() {
      return Array.from({length: len}, () => Math.floor(Math.random() * max));
    }
  };
}

