export const unsafeSwapImpl = function(i) {
  return function(j) {
    return function(l) {
      var x = l[i];
      l[i] = l[j];
      l[j] = x;
      return l;
    };
  };
}

