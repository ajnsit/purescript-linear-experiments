var message = "Error: Double use of linear value"

export function linear(val) {
  return { val, __PRIVATE__spent: false };
}

export function use(linear) {
  if (linear.__PRIVATE__spent) {
    throw new Error(message);
  }
  linear.__PRIVATE__spent = true;
  return linear.val;
}

export function read(linear) {
  return linear.val;
}

