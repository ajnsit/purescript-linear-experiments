module Quicksort where

import Data.Array as Array
import Data.Boolean (otherwise)
import Data.CommutativeRing ((+))
import Data.EuclideanRing ((-))
import Data.Maybe (Maybe(..))
import Data.Ord (class Ord, (<=), (>=))
import Linear (Linear, linear, read, use)

-- This does destructive updates
foreign import unsafeSwapImpl :: forall a. Int -> Int -> Array a -> Array a

-- This is safe because it uses linear
swap :: forall a. Int -> Int -> Linear (Array a) -> Linear (Array a)
swap i j l = linear (unsafeSwapImpl i j (use l))

qsort :: forall a. Ord a => Linear (Array a) -> Linear (Array a)
qsort arr = go arr 0 (Array.length (read arr) - 1)
  where
  go :: forall a. Ord a => Linear (Array a) -> Int -> Int -> Linear (Array a)
  go arr low high = do
    if low >= high then arr
    else do
      let { p, arr: arr2 } = partition arr high low Nothing
      let arr3 = go arr2 low (p - 1)
      go arr3 (p + 1) high

partition :: forall a. Ord a => Linear (Array a) -> Int -> Int -> Maybe Int -> { p :: Int, arr :: Linear (Array a) }
partition arr p i mj
  | i >= p = { arr, p }
  | otherwise =
      do
        -- `r` must never be wrapped in linear, nor escape this function
        let r = read arr
        let pv = Array.index r p
        let iv = Array.index r i
        if iv <= pv then partition arr p (i + 1) Nothing
        else go arr i (i + 1)

      where
      go :: forall a. Ord a => Linear (Array a) -> Int -> Int -> { p :: Int, arr :: Linear (Array a) }
      go arr i j
        | j >= p = { arr: swap p i arr, p: i }
        | otherwise = do
            -- `r` must never be wrapped in linear, nor escape this function
            let r = read arr
            let pv = Array.index r p
            let jv = Array.index r j
            if jv >= pv then go arr i (j + 1)
            else partition (swap i j arr) p (i + 1) Nothing

