module Linear (use, linear, read, Linear) where

data Linear :: forall k. k -> Type
data Linear a

foreign import use :: forall a. Linear a -> a
foreign import linear :: forall a. a -> Linear a
foreign import read :: forall a. Linear a -> a
